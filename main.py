from scraperUtil import *
import pandas as pd
import datetime
import os.path

#['diemen','zaandam','amsterdam'],
#["all","all","all"]

params={
	"cities":['amsterdam'],
	"listing_file":'10days',
	'scan':'buy',
	'scan_mode': ['all']
}

today = datetime.date.today().strftime("%Y-%m-%d")
scan = params['scan']

def get_browser():

	# profile = webdriver.FirefoxProfile()
	# profile.set_preference("general.useragent.override", "Googlebot/2.1+http://www.googlebot.com/bot.html)")
	# browser = webdriver.Firefox(profile)

	profile = webdriver.FirefoxProfile()
	profile.set_preference("general.useragent.override","'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36'")
	#profile.set_preference("general.useragent.override", "Googlebot/2.1+http://www.googlebot.com/bot.html)")
	profile.set_preference("javascript.enabled", True)
	PATH_TO_MY_PROFILE_CACHE = "/Users/cbruscantini/Library/Application\ Support/Firefox/Profiles/m8zv2uwf.default/"
	profile.set_preference("browser.cache.disk.parent_directory", PATH_TO_MY_PROFILE_CACHE)
	profile.set_preference("media.peerconnection.enabled", False)
	profile.set_preference("media.navigator.permission.disabled", True)
	profile.set_preference("browser.cache.disk.enable", True)
	profile.set_preference("browser.cache.memory.enable", True)
	profile.set_preference("browser.cache.offline.enable", True)
	profile.set_preference("network.http.use-cache", True)
	profile.set_preference("browser.cache.offline.parent_directory",PATH_TO_MY_PROFILE_CACHE)
	profile.set_preference("permissions.default.microphone", 1)
	profile.set_preference("permissions.default.camera", 1)
	profile.set_preference("dom.webnotifications.enabled", False)

	browser = webdriver.Firefox(profile)

	# opts = Options()
	# opts.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
	# browser = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=opts)
	# browser.set_page_load_timeout(30)
	return browser

def get_listing_data(funda,listing_namefile):
	if ((os.path.isfile(listing_namefile))and( params['listing_file']=='old' )):
		# loop over remaining links
		data1 = pd.read_csv(listing_namefile, sep = ',', encoding = 'utf-16')
		listingData = data1.to_dict('records')
	else:
		# Get the first page of the search, and wait for the javascript to render
		browser.get(funda)  
		sleep(5)
		sr = search_results_page(browser)

		# Past bot detection? get the pagination
		sr.get_pagination()
		links = sr.pagelist
		print "Nr of links: ",len(links)

		# create the array with data in it
		listingData = [search_result(listing).listing_data for listing in sr.listings]
		print "Nr of listingData: ",len(listingData)

		for page in links:
			print "page: ",page
			browser.get(page) 
			sr = search_results_page(browser)
			for listing in sr.listings:	
				try:
					listingData.append(search_result(listing).listing_data)
				except: 
					print "invalid record"
					pass
		data1 = df(listingData)
		if (params['listing_file']=='old'):
			with open(listing_namefile, 'a') as f:
				data1.to_csv(listing_namefile, sep = ',', encoding = 'utf-16', index = False)
		else:
			data1.to_csv(listing_namefile, sep = ',', encoding = 'utf-16', index = False)
	return listingData

browser=get_browser()

for city, scan_mode in zip(params['cities'],params['scan_mode']):

	funda_rent = "https://www.funda.nl/huur/{}/".format(city)
	funda_buy = "https://www.funda.nl/koop/{}/".format(city)
	if scan=='rent':
		funda = funda_rent
	else:
		funda = funda_buy
	if scan_mode=="5days":
		funda = funda+"5-dagen/"
	if scan_mode=="10days":
		funda = funda+"10-dagen/"

	listing_namefile = namefile('_listingData_'+scan+"_"+scan_mode+"_"+city)
	prop_filename = namefile('_propDATA_'+scan+"_"+scan_mode+"_"+city)

	listingData = get_listing_data(funda,listing_namefile)

	listingDetails=[]
	listingLinks = [d['href'] for d in listingData]
	listingAddresses = [d['address'] for d in listingData]

	for i in range(0,len(listingLinks)):
		print "%s getting %s" %(i, listingLinks[i])
		try:
			browser.get(listingLinks[i])
			l = listing_page(browser)
			l.data['address']= listingAddresses[i]
			l.data['href'] = listingLinks[i]
			listingDetails.append(l.data)
		except:
			print 'Exception - moving on to the next listing'
			pass
		if (i%50==0):
			data2 = df(listingDetails)
			print data2
			data2.to_csv(prop_filename, sep = ',', encoding = 'utf-16', index = False)
